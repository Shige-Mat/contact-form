@extends('app')

@section('content')
<main class="section">
	<div class="container">
		<div class="columns">
			<article class="column is-offset-2 is-8">
				@if(session()->has('message'))
					<div class="message is-success">
						<div class="message-body">
							<span class="help is-success title is-6">
								{{ session()->get('message') }}
							</span>
						</div>
					</div>
				@endif
				<div class="mb-1">
					<a href="{{ route('contact.index') }}" class="is-size-4">お問い合わせ一覧へ</a>
				</div>
				<h1 class="title is-3">お問い合わせ詳細</h1>

				<h2 class="title is-5 is-marginless">送信日時</h2>
				<p class="mb-1">
					{{ $contact->formatted_time }}
				</p>

				<h2 class="title is-5 is-marginless">名前</h2>
				<p class="mb-1">
					{{ $contact->full_name }}
				</p>

				<h2 class="title is-5 is-marginless">カナ</h2>
				<p class="mb-1">
					{{ $contact->full_name_kana }}
				</p>

				<h2 class="title is-5 is-marginless">性別</h2>
				<p class="mb-1">
					{{ $contact->gender == 'male' ? '男性' : '女性' }}
				</p>

				<h2 class="title is-5 is-marginless">電話番号</h2>
				<p class="mb-1">
					{{ $contact->phone_number }}
				</p>

				<h2 class="title is-5 is-marginless">誕生日</h2>
				<p class="mb-1">
					{{ \Carbon\Carbon::parse($contact->dob)->format('Y年m月d日') }}
				</p>

				@if($contact->body)
				<h2 class="title is-5 is-marginless">備考</h2>
				<p class="mb-1">
					{!! nl2br(e($contact->body)) !!}
				</p>
				@endif

				@if(count($contact->attachments))
				<h2 class="title is-5 mb-1 is-marginless">写真</h2>
				<p class="mb-1">
					@foreach($contact->attachments as $attachment)
						<figure class="image mb-1">
							<img src="{{ Storage::url($attachment->url) }}">
						</figure>
					@endforeach
				</p>
				@endif

				<div class="">
					<a href="{{ route('contact.index') }}" class="is-size-3">お問い合わせ一覧へ</a>
				</div>
			</article>
		</div>
	</div>
</main>
@endsection