@extends('app')

@section('content')
<main class="section">
	<div class="container">
		<div class="columns">
			<div class="column is-3">
				<div class="has-text-centered">

				<a href="{{ route('contact.create') }}" class="button is-info">お問い合わせ作成</a>
				</div>
			</div>
			<div class="column is-6">
				<h1 class="title is-4">お問い合わせ一覧</h1>
				@foreach($contacts as $contact)
					<article class="media">
						<div class="media-content">
							<a href="{{ route('contact.show', ['id' => $contact->id]) }}">
								{{ $contact->full_name }}さんからのお問い合わせ
							</a>
							<span class="help is-info is-marginless">{{ $contact->diff_time }}</span>
							@if($contact->body)
								{{ str_limit($contact->body) }}
							@endif
						</div>
					</article>
				@endforeach

				<div class="section">
				{{ $contacts->links() }}

				</div>
			</div>
		</div>
	</div>
</main>
@endsection