@extends('app')

@section('content')
<main class="section">
	<div class="container">
		<div class="columns">
			<section class="column is-offset-2 is-8">
				@if ($errors->any())
					<div class="message is-danger">
						<div class="message-body">
						<ul>
							@foreach ($errors->unique() as $error)
							<li>{{ $error }}</li>
							@endforeach
						</ul>
						</div>
					</div>
				@endif
				<div class="message is-dark is-medium">
				<div class="message-header">お問い合わせ</div>
					<div class="message-body is-white">
						<form action="{{ route('contact.store') }}" method="POST" enctype="multipart/form-data">
							{{ csrf_field() }}

						<div class="field">
							<div class="columns">
								<div class="column">
									<label for="last_name" class="label">性</label>
									<p class="control">
										<input class="input" type="text" name="last_name" placeholder="山田" value="{{ old('last_name') }}" >
									</p>
								</div>
								<div class="column">
									<label for="first_name" class="label">名</label>
									<p class="control">
										<input class="input" type="text" name="first_name" placeholder="太郎" value="{{ old('first_name') }}" >
									</p>
								</div>
							</div>
						</div>

						<div class="field">
							<div class="columns">
								<div class="column">
									<label for="last_name_kana" class="label">セイ</label>
									<p class="control  is-expanded">
										<input class="input" type="text" name="last_name_kana" placeholder="ヤマダ" value="{{ old('last_name_kana') }}" >
									</p>
								</div>

								<div class="column">
									<label for="first_name_kana" class="label">メイ</label>
									<p class="control is-expanded">
										<input class="input" type="text" name="first_name_kana" placeholder="タロウ" value="{{ old('first_name_kana') }}" >
									</p>
								</div>
							</div>
						</div>

						<div class="field">
							<label for="gender" class="label">性別</label>
							<div class="control">
								<label class="radio">
								<input type="radio" name="gender" value="male" {{ old('gender') == 'male' ? 'checked' : '' }}>
									男性
								</label>
								<label class="radio">
									<input type="radio" name="gender" value="female" {{ old('gender') == 'female' ? 'checked' : '' }}>
									女性
								</label>
							</div>
						</div>

						<div class="field">
							<label for="phone_number" class="label">電話番号</label>
							<span class="help is-info">すべて半角数字で入力してください</span>
							<div class="columns">

								<div class="column">
									<p class="control">
										<input class="input" type="text" name="phone_number[]" placeholder="03"
											value="{{ old('phone_number.0') }}" >
									</p>
								</div>
								<span class="dash">
									-
								</span>
								<div class="column">
									<p class="contro">
										<input class="input" type="text" name="phone_number[]" placeholder="1111" value="{{ old('phone_number.1') }}" >
									</p>
								</div>
								<span class="dash">
									-
								</span>

								<div class="column">
									<p class="control">
										<input class="input" type="text" name="phone_number[]" placeholder="1111" value="{{ old('phone_number.2') }}" >
									</p>
								</div>
							</div>
						</div>

						<div class="field">
							<label class="label" for="dob">誕生日</label>
							<span class="help is-info">例）2010/10/02</span>
							<p><input type="text" id="datepicker" class="input" name="dob" value="{{ old('dob') }}"></p>
						</div>

						<div class="field">
							<label class="label" for="body">備考欄</label>
							<p class="control">
								<textarea class="textarea" name="body" placeholder="お問い合わせ内容をご記入ください" >{{ old('body') }}</textarea>
							</p>
						</div>

						<div class="field mb-1">
							<label for="photo" class="label">写真</label>
							<p class="control mb-half">
								<input class="input" type="file" name="photo[]">
							</p>
							<p class="control">
								<input class="input" type="file" name="photo[]">
							</p>
						</div>


						<div class="control">
							<button class="button is-medium is-fullwidth is-primary">お問い合わせ送信</button>
						</div>

						</form>
					</div>
				</div>
			</section>
		</div>
	</div>
</main>

@push('footer-script')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="/js/datepicker-ja.js"></script>

<script>
$( function() {
    $( "#datepicker" ).datepicker({
      changeMonth: true,
      yearRange : '1900:2017',
      changeYear: true
    });
  } );
</script>

@endpush