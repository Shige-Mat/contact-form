<?php

Route::get('/','ContactsController@index')->name('contact.index');
Route::get('/create','ContactsController@create')->name('contact.create');
Route::post('/','ContactsController@store')->name('contact.store');
Route::get('/{contact}','ContactsController@show')->name('contact.show');
