<?php

namespace App;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
	protected $guarded = ['id'];


	public function contact()
	{
	    return $this->belongsTo('App\Contact');
	}

	public function upload(Request $request, $contactId)
	{
		if ($request->exists('photo')) {
			foreach ($request->photo as $photo) {
				$path = $photo->store('public/attachments/' . $contactId);

				(new self(['contact_id' => $contactId, 'url' => $path]))->save();
			}
		}
	}
}
