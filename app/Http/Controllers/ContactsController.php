<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Attachment;
use Illuminate\Http\Request;
use App\Http\Requests\ContactRequest;

class ContactsController extends Controller
{
	public function index()
	{
		$contacts = Contact::select('last_name', 'first_name', 'body', 'created_at', 'id')->latest()->paginate(25);

	    return view('contacts.index', compact('contacts'));
	}

	public function create()
	{
	    return view('contacts.create');
	}

	public function store(ContactRequest $request)
	{
		$contactId = app(Contact::class)->send($request);

		app(Attachment::class)->upload($request, $contactId);

		session()->flash('message', 'お問い合わせありがとうございます');

		return redirect(route('contact.show', $contactId));
	}

	public function show(Contact $contact)
	{
		return view('contacts.show', ['contact' => $contact->load('attachments')]);
	}
}
