<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|max:30|japanese',
            'last_name' => 'required|max:30|japanese',
            'first_name_kana' => 'required|max:30|kana',
            'last_name_kana' => 'required|max:30|kana',
            'phone_number.*' => 'required|numeric',
            'gender' => 'required|in:male,female',
            'photo.*' => 'mimes:jpeg,bmp,png|max:10240',
            'dob' => 'required|date_format:"Y/m/d"|after_or_equal:01 January 1900|before_or_equal:31 December 2017',
        ];
    }

    public function messages()
    {
        return [
            //KanaとJapaneseのルールはApp\Providers\validationProviderにあります
            'first_name.required' => '名前を入力してください',
            'first_name.max' => '名前を３０文字以内で入力してください',
            'first_name.japanese' => '日本語で名前を入力してください',
            'last_name.required' => '苗字を入力してください',
            'last_name.max' => '苗字を30文字以内で入力してください',
            'last_name.japanese' => '日本語で苗字をご記入ください',
            'first_name_kana.required' => 'メイを入力してください',
            'first_name_kana.max' => 'メイを３０文字以内で入力してください',
            'last_name_kana.required' => 'セイを入力してください',
            'last_name_kana.max' => 'セイを30文字以内で入力してください',
            'first_name_kana.kana' => 'メイをカタカナで入力してください',
            'last_name_kana.kana' => 'セイをカタカナで入力してください',
            'phone_number.*.required' => '電話番号の空欄を埋めてください',
            'phone_number.*.numeric' => '電話番号は全て半角数字で入力してください',
            'gender.required' => '性別を入力してください',
            'gender.in' => '性別をボタンから選んでください',
            'photo.*.mimes' => '写真はjpg, jpeg, png, bmp, gif, or svgのみアップロード可能です',
            'photo.*.max' => '写真は10mbまでアップロード可能です',
            'dob.required' => '誕生日を入力してください',
            'dob.date_format' => '誕生日は入力例に従って半角でご入力ください',
            'dob.before_or_equal' => '誕生日はカレンダーから選んでください',
            'dob.after_or_equal' => '誕生日はカレンダーから選んでください',
        ];
    }
}
