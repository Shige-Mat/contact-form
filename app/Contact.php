<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Contact extends Model
{
	protected $guarded = ['id'];


	public function attachments()
	{
	    return $this->hasMany('App\Attachment');
	}

	public function getFullNameAttribute()
	{
	    return $this->last_name . ' ' . $this->first_name;
	}

	public function getDiffTimeAttribute()
	{
	    return str_replace(' ', '', Carbon::parse($this->created_at)->diffForHumans());
	}

	public function getFormattedTimeAttribute()
	{
	    return Carbon::parse($this->created_at)->toDateTimeString();
	}

	public function getFullNameKanaAttribute()
	{
	    return $this->last_name_kana . ' ' . $this->first_name_kana;
	}

	public function send(Request $request)
	{
		$contact = $this->create([
			'first_name' => $request->first_name,
			'last_name' => $request->last_name,
			'first_name_kana' => $request->first_name_kana,
			'last_name_kana' => $request->last_name_kana,
			'dob' => $request->dob,
			'phone_number' => "{$request->phone_number[0]}-{$request->phone_number[1]}-{$request->phone_number[2]}",
			'body' => $request->body,
			'gender' => $request->gender,
		]);

		return $contact->id;
	}
}
