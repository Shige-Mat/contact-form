<?php

namespace App\Providers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class ValidationProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('kana', function ($attribute, $value, $parameters) {
            return preg_match('/^(?:[ァ-ヶ]?)+$/u', $value);
        });

        Validator::extend('japanese', function ($attribute, $value, $parameters) {
            return preg_match("/^(?:[ぁ-ゖァ-ヺㇰ-ㇿｦ-ｯｱ-ﾝ][\x{3099}\x{309A}]?|ー|[\p{Han}][\x{E0100}-\x{E01EF}\x{FE00}-\x{FE02}]?)+$/u", $value);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
